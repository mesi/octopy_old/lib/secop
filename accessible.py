import json
import traceback
from .node_base import NodeBase
from .datainfo import from_structure as datainfo_from_structure
from time import time


class Accessible(NodeBase):
    def __init__(self, conf):
        # Create member attributes
        self.full_id = None
        self.group = None
        self.visibility = None
        self.constant = None
        self.readonly = True
        self.datainfo = None
        self.value_topic = None
        self.get_topic = None
        self.set_topic = None
        self.ui_widgets = []
        # Update values from configuration
        self.deserialize(conf)
        # Member attributes needed for live data
        self._set_value = [ None, {} ]  # The last value that the accessible was set from user/controller
        if self.constant:
            self._source_value = [ self.constant, {} ]
            self._live = True
        else:
            self._source_value = [ None, {} ]  # The last value received from source/hardware
            self._live = False


    @property
    def parent(self):
        return self._parent


    def deserialize(self, conf):
        """Parses and validates a data structure and creates a new Accessible object from it"""
        super().deserialize(conf)
        self._parent = conf['parent']
        self.full_id = f"{self._parent.id}:{self.id}"
        self.group = self.validate_parameter(conf, param_name = 'group', param_type = str, required = False, node_name = self.full_name)
        self.visibility = self.validate_parameter(conf, param_name = 'visibility', param_type = str, required = False, node_name = self.full_name)
        self.constant = self.validate_parameter(conf, param_name = 'constant', param_type = bool, required = False, node_name = self.full_name)
        self.readonly = self.validate_parameter(conf, param_name = 'readonly', param_type = bool, required = True, node_name = self.full_name)
        # Datainfo
        datainfo_conf = self.validate_parameter(conf, param_name = 'datainfo', param_type = dict, required = True, node_name = self.full_name)
        try:
            datainfo = datainfo_from_structure(datainfo_conf)
        except Exception as err:
            raise ValueError(f"Could not parse datainfo for '{self.full_name}': {err}")
        self.datainfo = datainfo
        # Octopy-sepcific parameters
        octopy_conf = self.validate_parameter(conf, param_name = 'octopy', param_type = dict, required = False, node_name = self.full_name)
        if octopy_conf:
            links = self.validate_parameter(octopy_conf, param_name = 'links', param_type = dict, required = False, node_name = self.full_name)
            if links:
                self.get_topic = self.validate_parameter(links, param_name = 'get topic', param_type = str, required = False, node_name = self.full_name)
                self.set_topic = self.validate_parameter(links, param_name = 'set topic', param_type = str, required = False, node_name = self.full_name)
                self.value_topic = self.validate_parameter(links, param_name = 'value topic', param_type = str, required = False, node_name = self.full_name)
            hints = self.validate_parameter(octopy_conf, param_name = 'hints', param_type = dict, required = False, node_name = self.full_name)
            if hints:
                ui_hints = self.validate_parameter(hints, param_name = 'ui', param_type = dict, required = False, node_name = self.full_name)
                if ui_hints:
                    self.ui_widgets = self.validate_parameter(ui_hints, param_name = 'widgets', param_type = list, required = False, node_name = self.full_name)


    def module(self):
        return self._parent


    @property
    def source_value(self):
        return self._source_value


    @source_value.setter
    def source_value(self, value):
        value = self.validate_value(value)
        self._source_value = value
        self._live = True
        return self._source_value


    @property
    def set_value(self):
        if self.readonly:
            raise ValueError(f"Can't set readonly accessible")
        return self._set_value


    @set_value.setter
    def set_value(self, value):
        value = self.validate_value(value)
        self._set_value = value
        return self._set_value


    @property
    def has_live_data(self):
        return self._live


    def validate_value(self, value):
        if not isinstance(value, list):
            raise TypeError(f"Value is not in SECoP format. Expected list. Got {val.__class__.__name__}.")
        new_value = value[0]
        if len(value) > 1:
            qualifiers = value[1]
            if not isinstance(qualifiers, dict):
                raise TypeError(f"Value is not in SECoP format. Qualifier element should be a dict, but is {qualifiers.__class__.__name__}.")
        else:
            qualifiers = {}
        # Make sure the data is timestamped
        if not 't' in qualifiers:
            qualifiers['t'] = time()
        # Validate the value against the set SECoP type and settings
        new_value = self.datainfo.validate_value(new_value)
        return [ new_value, qualifiers ]


    def serialize(self, pure_secop = False):
        """Converts the object into a data strcture that can be further converted into JSON"""
        structure = {
            'description': self.description,
            'readonly': self.readonly,
            'visibility': self.visibility,
            'group': self.group,
            'constant': self.constant,
            'datainfo': self.datainfo.serialize()
        }
        if not pure_secop:
            structure['octopy'] = {
                'links': {
                    'value topic': self.value_topic,
                    'set topic': self.set_topic,
                    'get topic': self.get_topic
                }
            }
            if hasattr(self, 'ui_widgets'):
                structure['octopy']['hints'] = {
                    'ui': {
                        'widgets': self.ui_widgets
                    }
                }
        return structure
