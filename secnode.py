from .node_base import NodeBase
from .module import Module
from .module import Accessible


class SecNode(NodeBase):
    DEFAULT_ACCESSIBLE_NAME = 'target'

    def __init__(self, conf, accessible_class = Accessible):
        conf['id'] = conf.get('equipment_id')
        self.equipment_name = None
        self.firmware = None
        self.implementor = None
        self.timeout = None
        self.modules = {}
        self._accessible_class = accessible_class
        self.deserialize(conf)


    def deserialize(self, conf):
        """Parses and validates a data structure and creates a new SecNode object from it"""
        self._value_topics_cache = None
        self._set_topics_cache = None
        self._get_topics_cache = None
        super().deserialize(conf)
        self.equipment_id = self.validate_parameter(conf, param_name = 'equipment_id', param_type = str, required = True, node_name = self.name)
        self.firmware = self.validate_parameter(conf, param_name = 'firmware', param_type = str, required = False, node_name = self.name)
        self.implementor = self.validate_parameter(conf, param_name = 'implementor', param_type = str, required = False, node_name = self.name)
        self.timeout = self.validate_parameter(conf, param_name = 'timeout', param_type = [int, float], required = False, node_name = self.name)
        mods_conf = self.validate_parameter(conf, param_name = 'modules', param_type = dict, required = True, node_name = self.name)
        self._accessibles_cache = None
        self.deserialize_modules(mods_conf)


    def deserialize_modules(self, modules_conf):
        # Create/update modules
        for mod_id, mod_conf in modules_conf.items():
            mod_conf['parent'] = self
            mod_conf['id'] = mod_id
            if mod_id in self.modules:
                self.modules[mod_id].deserialize(mod_conf)
            else:
                self.modules[mod_id] = Module(mod_conf, accessible_class = self._accessible_class)
        # Remove modules no longer present in conf
        for mod_id in self.modules.copy():
            if not mod_id in modules_conf:
                del self.modules[mod_id]


    def serialize(self, pure_secop = False):
        """Converts the object into a data strcture that can be further converted into JSON"""
        structure = {
            'description': self.description,
            'equipment_id': self.equipment_id,
            'firmware': self.firmware,
            'implementor': self.implementor,
            'timeout': self.timeout,
            'modules': {}
        }
        if not pure_secop:
            # Add Octopy-specific data here
            pass
        # Serialize modules
        for mod_id, mod  in self.modules.items():
            structure['modules'][mod_id] = mod.serialize(pure_secop)
        return structure


    @property
    def accessibles(self):
        """Helper method that returns a flat list of all accessibles in all modules"""
        if not self._accessibles_cache:
            accessibles = {}
            for mod_id, module in self.modules.items():
                accessibles.update(module.accessibles)
            self._accessibles_cache = accessibles
        return self._accessibles_cache


    @property
    def value_topics(self):
        """Helper method that returns a list of all value_topic attributes and
        the accessibles they were found in."""
        # First check if the cache exists
        if not self._value_topics_cache:
            ### Find all value_topics in all accessibles
            value_topics = {}
            for mod_id, module in self.modules.items():
                for acc_id, accessible in module.accessibles.items():
                    if hasattr(accessible, 'value_topic'):
                        # Accessible has value_topic
                        topic = accessible.value_topic
                        if not topic in value_topics:
                            value_topics[topic] = []
                        value_topics[topic].append(accessible)
            self._value_topics_cache = value_topics
        return self._value_topics_cache


    @property
    def set_topics(self):
        """Helper method that returns a list of all set_topic attributes and
        the accessibles they were found in."""
        if not self._set_topics_cache:
            ### Find all set_topics in all accessibles
            set_topics = {}
            for mod_id, module in self.modules.items():
                for acc_id, accessible in module.accessibles.items():
                    if  hasattr(accessible, 'set_topic'):
                        # Accessible has value_topic
                        topic = accessible.set_topic
                        if not topic in set_topics:
                            set_topics[topic] = []
                        set_topics[topic].append(accessible)
            self._set_topics_cache = set_topics
        return self._set_topics_cache


    @property
    def get_topics(self):
        """Helper method that returns a list of all get_topic attributes and
        the accessibles they were found in."""
        if not self._get_topics_cache:
            ### Find all get_topics in all accessibles
            get_topics = {}
            for mod_id, module in self.modules.items():
                for acc_id, accessible in module.accessibles.items():
                    if  hasattr(accessible, 'get_topic'):
                        # Accessible has value_topic
                        topic = accessible.get_topic
                        if not topic in get_topics:
                            get_topics[topic] = []
                        get_topics[topic].append(accessible)
            self._get_topics_cache = get_topics
        return self._get_topics_cache



class Node(SecNode):
    """Temporary Node-class for compatibility until code that uses it has been updated to use SecNode"""
    pass


if __name__ == '__main__':
    import json
    json_node = """
{

    "equipment_id":"test",
    "description":"2 individually controlled temperature controlled fluid baths with circulation and setpoints.",
    "firmware":"123.456",
    "implementor":"nothin",
    "timeout":12,
    "modules":{
        "Tctrl_01":{
            "description":"A temperature controlled fluid bath with circulation (over EPICS).",
            "interface_classes":[
                "drivable"
            ],
            "accessibles":{
                "value":{
                    "datainfo":{
                        "type":"double",
                        "min":0,
                        "max":100,
                        "unit":"C"
                    },
                    "readonly":true,
                    "group":"tempgroup1",
                    "description":"Waterbath internal Temperature",
                    "octopy":{
                        "links": {
                            "get_reference":"J2:TEMP",
                            "value_topic":"julabo1/Tctrl/value"
                        }
                    }
                },
                "target":{
                    "datainfo":{
                        "type":"double",
                        "min":0,
                        "max":100,
                        "unit":"C"
                    },
                    "readonly":false,
                    "group":"tempgroup1",
                    "description":"temperature target",
                    "octopy":{
                        "links": {
                            "get_topic":"julabo1/Tctrl/target",
                            "set_topic":"julabo1/Tctrl/target/set",
                            "get_reference":"J2:TEMP:SP1:RBV",
                            "set_reference":"J2:TEMP:SP1"
                        }
                    },
                    "ECS_link":{
                        "auto_topic":"julabo1/Tctrl/target",
                        "get_topic":"",
                        "get_reference":"",
                        "set_topic":"",
                        "set_reference":"julabo1:Tctrl:target:set",
                        "value_topic":"",
                        "datatype":"double",
                        "priority":1,
                        "create_pv":true
                    }
                },
                "stop":{
                    "datainfo":{
                        "type":"command"
                    },
                    "readonly":false,
                    "octopy": {
                        "links":{
                            "get_topic":"epicsjulabo/stop/get",
                            "set_topic":"epicsjulabo/stop/set",
                            "value_topic":"epicsjulabo/stop/value"
                        }
                    },
                    "description":"Stop it"
                },
                "status":{
                    "datainfo":{
                        "type":"tuple",
                        "members":[
                            {
                                "type":"enum",
                                "members":{
                                    "IDLE":100,
                                    "BUSY":300
                                }
                            },
                            {
                                "type":"string"
                            }
                        ]
                    },
                    "readonly":true,
                    "description":"Module status"
                },
                "circulation":{
                    "datainfo":{
                        "type":"enum",
                        "members":{
                            "OFF":0,
                            "ON":1
                        }
                    },
                    "readonly":false,
                    "group":"tempgroup2",
                    "description":"start/stop the circulation and controller"
                }
            }
        },
        "Tctrl_02":{
            "description":"A temperature controlled fluid bath with circulation (over Socket).",
            "interface_classes":[
                "drivable"
            ],
            "accessibles":{
                "value":{
                    "datainfo":{
                        "type":"double",
                        "min":0,
                        "max":100,
                        "unit":"C"
                    },
                    "readonly":true,
                    "group":"tempgroup2",
                    "description":"Waterbath internal Temperature"
                },
                "target":{
                    "datainfo":{
                        "type":"double",
                        "min":0,
                        "max":100,
                        "unit":"C"
                    },
                    "readonly":false,
                    "group":"tempgroup2",
                    "description":"temperature target"
                },
                "stop":{
                    "datainfo":{
                        "type":"command"
                    },
                    "readonly": true,
                    "description":"Stop it"
                },
                "status":{
                    "datainfo":{
                        "type":"tuple",
                        "members":[
                            {
                                "type":"enum",
                                "members":{
                                    "IDLE":100,
                                    "BUSY":300
                                }
                            },
                            {
                                "type":"string"
                            }
                        ]
                    },
                    "readonly":true,
                    "description":"Module status"
                },
                "circulation":{
                    "datainfo":{
                        "type":"enum",
                        "members":{
                            "OFF":0,
                            "ON":1
                        }
                    },
                    "readonly":false,
                    "group":"tempgroup2",
                    "description":"start/stop the circulation and controller"
                }
            }
        }
    }

}
"""
    my_node = SecNode(json.loads(json_node))
    print(my_node)
