import json
from . import errors as secop_errors

class NodeBase:
    def deserialize(self, conf):
        self.description = self.validate_parameter(conf, param_name = 'description', param_type = str, required = True)
        self.name = self.description.split("\n\n")[0]
        self.id = conf['id']


    @property
    def full_name(self):
        return self.path()


    def path(self, separator = ':'):
        if hasattr(self, '_parent') and self._parent:
            return f"{self._parent.full_name}{separator}{self.name}"
        else:
            return self.name


    def validate_parameter(self, conf, param_name, param_type, required = True, node_name = None):
        if required and not param_name in conf:
            if node_name:
                raise secop_errors.NoSuchParameterError(f"required parameter '{param_name}' is missing in '{node_name}'")
            else:
                raise secop_errors.NoSuchParameterError(f"required parameter '{param_name}' is missing")
        elif not required and not param_name in conf:
            return None
        param = conf[param_name]
        if not isinstance(param_type, list):
            param_type = [param_type]
        correct_type = True
        for type in param_type:
            if isinstance(param, type):
                correct_type = True
        if not correct_type:
            raise secop_errors.WrongTypeError(f"parameter '{param_name}' is the wrong type. Got {param.__class__.__name__}, expected {type.__name__}")
        return param


    def structure(self):
        """Alias for serialize(). Needed for compatibility. Should probably be removed in the future."""
        return self.serialize(pure_secop = False)


    def __str__(self):
        return json.dumps(self.serialize(pure_secop = False))

