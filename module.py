from .node_base import NodeBase
from .accessible import Accessible


class Module(NodeBase):
    def __init__(self, conf, accessible_class = Accessible):
        self.interface_classes = []
        self.visibility = None
        self.group = None
        self.meaning = None
        self.implementor = None
        self._accessibles = {}
        self._accessible_class = accessible_class
        self.deserialize(conf)


    def deserialize(self, conf):
        """Parses and validates a data structure and creates a new Module object from it"""
        super().deserialize(conf)
        self._parent = conf['parent']
        # SECoP-specific parameters
        self.interface_classes = self.validate_parameter(conf, param_name = 'interface_classes', param_type = list, required = True, node_name = self.full_name)
        # The interface class list must not be empty. If it is we add a default class of Readable.
        if len(self.interface_classes) == 0:
            self.interface_classes.append('Readable')
        self.visibility = self.validate_parameter(conf, param_name = 'visibility', param_type = str, required = False, node_name = self.full_name)
        self.group = self.validate_parameter(conf, param_name = 'group', param_type = str, required = False, node_name = self.full_name)
        self.meaning = self.validate_parameter(conf, param_name = 'meaning', param_type = list, required = False, node_name = self.full_name)
        self.implementor = self.validate_parameter(conf, param_name = 'implementor', param_type = str, required = False, node_name = self.full_name)
        accs_conf = self.validate_parameter(conf, param_name = 'accessibles', param_type = dict, required = True, node_name = self.full_name)
        self.deserialize_accessibles(accs_conf)


    def deserialize_accessibles(self, accessibles_conf):
        # Create/update accessibles
        for acc_id, acc_conf in accessibles_conf.items():
            acc_conf['parent'] = self
            acc_conf['id'] = acc_id
            if acc_id in self._accessibles:
                self._accessibles[acc_id].deserialize(acc_conf)
            else:
                self._accessibles[acc_id] = self._accessible_class(acc_conf)
        # Remove accessibles no longer present in conf
        for acc_id in self._accessibles.copy():
            if not acc_id in accessibles_conf:
                del self._accessibles[acc_id]
        # If no status accessible exists we create a default one
        if not 'status' in self._accessibles:
            self.create_status_accessible()

    def create_status_accessible(self):
        value_topic = '/status'
        if 'value' in self._accessibles:
            value_topic = self._accessibles['value'].value_topic + value_topic
        elif len(self._accessibles) > 0 and hasattr(list(self._accessibles.values())[0], 'value_topic'):
            value_topic = list(self._accessibles.values())[0].value_topic + value_topic
        else:
            value_topic = self.id + value_topic
        conf = {
            'id': 'status',
            'description': 'Status',
            'parent': self,
            'readonly': True,
            'octopy': {
                'links': {
                    'value topic': value_topic
                }
            },
            'datainfo': {
                'type': 'tuple',
                'members': [
                    {
                        'type': 'int',
                        'min': 0,
                        'max': 499
                    },
                    {
                        'type': 'string',
                        'maxchars': 80
                    }
                ]
            },
            'constant': 100   # Temporary hack until we have proper status functionality
        }
        self._accessibles['status'] = self._accessible_class(conf)


    def serialize(self, pure_secop = False):
        """Converts the object into a data strcture that can be further converted into JSON"""
        structure = {
            'description': self.description,
            'interface_classes': self.interface_classes,
            'visibility': self.visibility,
            'group': self.group,
            'meaning': self.meaning,
            'implementor': self.implementor,
            'accessibles': {}
        }
        if not pure_secop:
            # Add Octopy-specific data here
            pass
        # Serialize accessibles
        for acc_id, acc  in self._accessibles.items():
            structure['accessibles'][acc_id] = acc.serialize(pure_secop)
        return structure


    @property
    def parent(self):
        return self._parent


    @property
    def accessibles(self):
        accessibles = {}
        for accessible in self._accessibles.values():
            accessibles[accessible.full_id] = accessible
        return accessibles


    @property
    def node(self):
        return self._parent

