import re
import json
import numbers
from . import errors as secop_errors

FMT_STR = re.compile('^%[.][0-9]+')
NBR_STR = re.compile('^[0-9\.]+')


def from_structure(structure):
    """Returns datainfo object from a SECoP datainfo structure"""
    if not isinstance(structure, dict):
        msg = f"Invalid data type for datainfo configuration. Expected dict. Got {structure.__class__.__name__}."
        raise TypeError(msg)
    if not ('type' in structure) or not structure['type']:
        raise ValueError(f"Datainfo config is missing key 'type': {structure}")
    datainfo_type = structure['type']
    if not (datainfo_type in SECOP_TYPES):
        raise ValueError(f"Unsupported/unknown data type in datainfo conf: {datainfo_type}")
    data_info_class = SECOP_TYPES[datainfo_type]
    return data_info_class(structure)


def suggest(data, parse_from_string=False):
    """Analyzing data to find out how it could fit into SECoP data types and returning the suggested class"""

    if isinstance(data, bool):
        return SecBool()
    if isinstance(data, int):
        return SecDouble()
    if isinstance(data, float):
        return SecDouble()
    if isinstance(data, dict):
        members = dict()
        for k, v in data.items():
            members[k] = suggest(v, parse_from_string).structure()
        return SecStruct({
            'members': members
        })
    if isinstance(data, list):
        members = suggest(data[0], parse_from_string).structure()
        return SecArray({
            'members': members,
            'maxlen': len(data)
        })
    if isinstance(data, str):
        if not parse_from_string:
            return SecString()
        if data.startswith('+') or data.startswith('-'):
            data = data[1:]
        if NBR_STR.match(data):
            decimals = data.count('.')
            if decimals == 1:
                return SecDouble()
            elif decimals == 0:
                return SecDouble()
            else:
                return SecString()
    print('suggest yielded no match for: {}'.format(data))
    return SecString()


class DataInfo:
    def __init__(self, conf = {}):
        self.deserialize(conf)
        self._value = None

    def deserialize(self, conf):
        pass

    def serialize(self):
        pass

    def structure(self):
        return self.serialize()

    def __str__(self):
        return str(self.serialize())

    def validate_value(self, val):
        """Validates that a value conforms to the type and limits"""
        pass

    def validate_parameter(self, conf, param_name, param_type=None, required=True):
        if required and param_name not in conf:
            raise secop_errors.NoSuchParameterError(f"required parameter '{param_name}' is missing")
        elif not required and not param_name in conf:
            return None
        param = conf[param_name]
        if not isinstance(param_type, list):
            param_type = [param_type]
        correct_type = True
        for ptype in param_type:
            if isinstance(param, ptype):
                correct_type = True
        if not correct_type:
            raise secop_errors.WrongTypeError(f"parameter '{param_name}' is the wrong type. Got {param.__class__.__name__}, expected {type.__name__}")
        return param

    def validate_fmtstr(self):
        if self.fmtstr and not FMT_STR.search(self.fmtstr):
            raise secop_errors.WrongTypeError(f"parameter 'fmtstr' has an invalid format: '{self.fmtstr}'")


class SecDouble(DataInfo):
    def __init__(self, conf = {}):
        self.type = 'double'
        super().__init__(conf)

    def deserialize(self, conf):
        self.min = self.validate_parameter(conf, param_name='min', param_type=(int, float), required=False)
        self.max = self.validate_parameter(conf, param_name='max', param_type=(int, float), required=False)
        self.unit = self.validate_parameter(conf, param_name='unit', param_type=str, required=False)
        self.absolute_resolution = self.validate_parameter(conf, param_name='absolute_resolution',
                                                           param_type=(int, float), required=False)
        self.relative_resolution = self.validate_parameter(conf, param_name='relative_Resolution',
                                                           param_type=(int, float), required=False)
        self.fmtstr = self.validate_parameter(conf, param_name='fmtstr', param_type=str, required=False)
        self.validate_fmtstr()
        self.clamp_on_validation = self.validate_parameter(conf, param_name='clamp on validation', param_type=(int, float), required=False)

    def serialize(self):
        structure  = {
            'type': 'double',
            'min': self.min,
            'max': self.max,
            'unit': self.unit,
            'absolute_resolution': self.absolute_resolution,
            'relative_resolution': self.relative_resolution,
            'fmtstr': self.fmtstr
        }
        return structure

    def validate_value(self, val):
        """Validates that a value conforms to the type and limits"""
        if val is None:
            raise secop_errors.WrongTypeError(f"Incorrect value. Expected a number (float). Got {val.__class__name}: '{str(val)}'.")
        multiplier = 1
        if isinstance(val, str):
            if val.startswith('+'):
                val = val[1:]
                pass
            if val.startswith('-'):
                val = val[1:]
                multiplier = -1
                pass
        try:
            val = float(val) * multiplier
        except ValueError as err:
            raise secop_errors.WrongTypeError(f"Incorrect value. Expected a number (float). Got '{str(val)}'.")
        except TypeError as err:
            raise secop_errors.WrongTypeError(f"Invalid data type. Expected 'Number'. Got {val.__class__.__name__}: '{str(val)}'")
        if isinstance(self.min, numbers.Number) and val < self.min:
            if self.clamp_on_validation:
                return self.min
            else:
                raise secop_errors.RangeError(f"Value {val} is too small. Must be in range [{self.min}, {self.max}].")
        if isinstance(self.max, numbers.Number) and val > self.max:
            if self.clamp_on_validation:
                return self.max
            else:
                raise secop_errors.RangeError(f"Value {val} is too large. Must be in range [{self.min}, {self.max}].")
        return val


class SecInt(DataInfo):
    def __init__(self, conf = {}):
        self.type = 'int'
        super().__init__(conf)

    def deserialize(self, conf):
        if 'min' not in conf.keys():
            conf['min'] = -pow(2, 24)
        if 'max' not in conf.keys():
            conf['max'] = pow(2, 24)
        self.min = self.validate_parameter(conf, param_name='min', param_type=(int, float), required=True)
        self.max = self.validate_parameter(conf, param_name='max', param_type=(int, float), required=True)
        self.unit = self.validate_parameter(conf, param_name='unit', param_type=str, required=False)
        self.clamp_on_validation = self.validate_parameter(conf, param_name='clamp on validation', param_type=(int, float), required=False)

    def serialize(self):
        structure  = {
            'type': 'int',
            'min': self.min,
            'max': self.max,
            'unit': self.unit
        }
        return structure

    def validate_value(self, val):
        """Validates that a value conforms to the type and limits"""
        multiplier = 1
        if isinstance(val, str):
            if val.startswith('+'):
                val = val[1:]
                pass
            if val.startswith('-'):
                val = val[1:]
                multiplier = -1
                pass
        try:
            val = int(val) * multiplier
        except ValueError as err:
            raise secop_errors.WrongTypeError(f"Incorrect value. Expected a number (int). Got {val}.")
        except TypeError as err:
            raise secop_errors.WrongTypeError(f"Invalid data type. Expected 'Number'. Got {val.__class__.__name__}")
        if isinstance(self.min, numbers.Number) and val < self.min:
            if self.clamp_on_validation:
                return self.min
            else:
                raise secop_errors.RangeError(f"Value {val} is too small. Must be in range [{self.min}, {self.max}].")
        if isinstance(self.max, numbers.Number) and val > self.max:
            if self.clamp_on_validation:
                return self.max
            else:
                raise secop_errors.RangeError(f"Value {val} is too large. Must be in range [{self.min}, {self.max}].")
        return val


class SecScaled(DataInfo):
    def __init__(self, conf = {}):
        self.type = 'scaled'
        super().__init__(conf)

    def deserialize(self, conf):
        self.scale = self.validate_parameter(conf, param_name='scale', param_type=(int, float), required=True)
        self.min = self.validate_parameter(conf, param_name='min', param_type=int, required=True)
        self.max = self.validate_parameter(conf, param_name='max', param_type=int, required=True)
        self.unit = self.validate_parameter(conf, param_name='unit', param_type=str, required=False)
        self.absolute_resolution = self.validate_parameter(conf, param_name='absolute_resolution',
                                                           param_type=(int, float), required=False)
        self.relative_resolution = self.validate_parameter(conf, param_name='relative_Resolution',
                                                           param_type=(int, float), required=False)
        self.fmtstr = self.validate_parameter(conf, param_name='fmtstr', param_type=str, required=False)
        self.validate_fmtstr()
        self.clamp_on_validation = self.validate_parameter(conf, param_name='clamp on validation', param_type=(int, float), required=False)

    def serialize(self):
        structure = {
            'type': 'scaled',
            'scale': self.scale,
            'min': self.min,
            'max': self.max,
            'unit': self.unit,
            'absolute_resolution': self.absolute_resolution,
            'relative_resolution': self.relative_resolution,
            'fmtstr': self.fmtstr
        }
        return structure

    def validate_value(self, val):
        """Validates that a value conforms to the type and limits"""
        try:
            val = int(float(val)*self.scale)
        except ValueError as err:
            raise secop_errors.WrongTypeError(f"Incorrect value. Expected a number (int). Got {val}.")
        except TypeError as err:
            raise secop_errors.WrongTypeError(f"Invalid data type. Expected 'Number'. Got {val.__class__.__name__}")
        if isinstance(self.min, numbers.Number) and val < self.min:
            if self.clamp_on_validation:
                return self.min
            else:
                raise secop_errors.RangeError(f"Value {val} is too small. Must be in range [{self.min}, {self.max}].")
        if isinstance(self.max, numbers.Number) and val > self.max:
            if self.clamp_on_validation:
                return self.max
            else:
                raise secop_errors.RangeError(f"Value {val} is too large. Must be in range [{self.min}, {self.max}].")
        return val


class SecBool(DataInfo):
    def __init__(self, conf = {}):
        self.type = 'bool'
        super().__init__(conf)

    def serialize(self):
        structure  = {
            'type': 'bool'
        }
        return structure

    def validate_value(self, val):
        """Validates that a value conforms to the type and limits"""
        if isinstance(val, str):
            if val == 'false':
                val = False
            if val == 'true':
                val = True
        try:
            val = bool(val)
        except ValueError as err:
            raise secop_errors.WrongTypeError(f"Incorrect value. Expected a boolean. Got {val}.")
        except TypeError as err:
            raise secop_errors.WrongTypeError(f"Invalid data type. Expected 'bool'. Got {val.__class__.__name__}")
        return val


class SecString(DataInfo):
    def __init__(self, conf = {}):
        self.type = 'string'
        super().__init__(conf)

    def deserialize(self, conf):
        self.minchars = self.validate_parameter(conf, param_name='minchars', param_type=int, required=False)
        self.maxchars = self.validate_parameter(conf, param_name='maxchars', param_type=int, required=False)
        self.isUTF8 = self.validate_parameter(conf, param_name='isUTF8', param_type=bool, required=False)

    def serialize(self):
        structure  = {
            'type': 'string',
            'minchars': self.minchars,
            'maxchars': self.maxchars,
            'isUTF8': self.isUTF8
        }
        return structure

    def validate_value(self, val):
        """Validates that a value conforms to the type and limits"""
        try:
            ret = str(val)
        except ValueError as err:
            raise secop_errors.WrongTypeError(f"Incorrect value. Expected a string. Got {val}.")
        return ret


class SecArray(DataInfo):
    def __init__(self, conf = {}):
        self.type = 'array'
        super().__init__(conf)

    def deserialize(self, conf):
        members = self.validate_parameter(conf, param_name='members', param_type=dict, required=True)
        self._members = from_structure(members)
        self.maxlen = self.validate_parameter(conf, param_name='maxlen', param_type=int, required=True)
        self.minlen = self.validate_parameter(conf, param_name='minlen', param_type=int, required=False)

    def serialize(self):
        structure  = {
            'type': 'array',
            'minlen': self.minlen,
            'maxlen': self.maxlen,
            'members': self._members.serialize()
        }
        return structure

    @property
    def members_type(self):
        return self._members.type

    @property
    def members(self):
        return self._members

    def validate_value(self, val):
        """Validates that a value conforms to the type and limits"""
        if not isinstance(val, list):
            raise secop_errors.WrongTypeError(f"Expected 'list'. Got {val.__class__.__name__}.")
        if self.minlen and len(val) < self.minlen:
            raise secop_errors.RangeError(f"Array is too short. Must be in range [{self.minlen}, {self.maxlen}].")
        if self.maxlen and len(val) > self.maxlen:
            raise secop_errors.RangeError(f"Array is too long. Must be in range [{self.minlen}, {self.maxlen}].")
        ret = []
        for element in val:
            ret.append(self._members.validate_value(element))
        return ret


class SecStruct(DataInfo):
    def __init__(self, conf = {}):
        self.type = 'struct'
        super().__init__(conf)

    def deserialize(self, conf):
        members = self.validate_parameter(conf, param_name='members', param_type=dict, required=True)
        self._members = {}
        for key, member_conf in members.items():
            member = from_structure(member_conf)
            self._members[key] = member
        if len(self._members) == 0:
            raise ValueError(f"SecStruct has no members")
        self._optional = self.validate_parameter(conf, param_name='optional', param_type=list, required=False)
        if not self._optional:
            self._optional = []

    def serialize(self):
        structure  = {
            'type': 'struct',
            'optional': self._optional,
            'members': {}
        }
        for key, member in self._members.items():
            structure['members'][key] = member.serialize()
        return structure

    @property
    def members(self):
        return self._members

    def add_member(self, key, member):
        if issubclass(type(member), DataInfo):
            if isinstance(key, str):
                self._members.update({key: member.structure()})
            else:
                raise secop_errors.WrongTypeError('A SecStruct member has to have a key')
        else:
            raise secop_errors.WrongTypeError('Data type has to belong to the DataInfo class')

    def validate_value(self, val):
        """Validates that a value conforms to the type and limits"""
        if not isinstance(val, dict):
            raise secop_errors.WrongTypeError(f"Expected 'dict'. Got {val.__class__.__name__}.")
        ret = {}
        for member_name, member_value in val.items():
            if not member_name in self._members:
                raise secop_errors.WrongTypeError(f"Struct value contains a member that is not part of the structure: '{member_name}'")
            member = self._members[member_name]
            ret[member_name] = member.validate_value(member_value)
        for member_name in self._members.keys():
            if not member_name in self._optional and not member_name in ret:
                raise secop_errors.WrongTypeError(f"Struct value is missing a non-optional member: '{member_name}'")
        return ret


class SecEnum(DataInfo):
    def __init__(self, conf = {}):
        self.type = 'enum'
        self._py_types = dict
        super().__init__(conf)

    def deserialize(self, conf):
        self._members = self.validate_parameter(conf, param_name='members', param_type=dict, required=True)
        if len(self._members) == 0:
            raise ValueError("Enum has no members")

    def serialize(self):
        structure  = {
            'type': 'enum',
            'members': self._members
        }
        return structure

    @property
    def members(self):
        return self._members

    def validate_value(self, val):
        """Validates that a value conforms to the type and limits"""
        try:
            ret = int(val)
        except ValueError as err:
            raise secop_errors.WrongTypeError(f"Incorrect value. Expected an int. Got {val}.")
        except TypeError as err:
            raise secop_errors.WrongTypeError(f"Invalid data type. Expected 'int'. Got {val.__class__.__name__}")
        if ret not in self._members.values():
            raise secop_errors.RangeError(f"The value {val} is not part of the enum. Must be one of [str(self._members.values())]")
        return ret


class SecTuple(DataInfo):
    def __init__(self, conf = {}):
        self.type = 'tuple'
        self._py_types = (tuple, list)
        self._inner_types = list()
        super().__init__(conf)

    def deserialize(self, conf):
        members = self.validate_parameter(conf, param_name='members', param_type=list, required=True)
        self._members = []
        for member_conf in members:
            member = from_structure(member_conf)
            self._members.append(member)

    def serialize(self):
        structure  = {
            'type': 'tuple',
            'members': []
        }
        for member in self._members:
            structure['members'].append(member.serialize())
        if len(self._members) != 2:
            raise(ValueError, f"Tuple must have exactly two members. Got {len(self._members)}.")
        return structure

    @property
    def members(self):
        return self._members

    def validate_value(self, val):
        """Validates that a value conforms to the type and limits"""
        if not isinstance(val, list):
            raise secop_errors.WrongTypeError(f"Expected 'list'. Got {val.__class__.__name__}.")
        if len(val) < 2:
            raise secop_errors.RangeError(f"Tuple has too few values. Tuples must have exactly 2 value.")
        if len(val) > 2:
            raise secop_errors.RangeError(f"Tuple has too many values. Tuples must have exactly 2 value.")
        ret = []
        for element in val:
            ret.append(self._members.validate_value(element))
        return ret


class SecCommand(DataInfo):
    def __init__(self, conf = {}):
        self.type = 'command'
        super(SecCommand, self).__init__(conf)
        self.argument = from_structure(kwargs.get('argument'))
        self.result = from_structure(kwargs.get('result'))
        # if not self.argument or not self.result:
        #     raise errors.ProtocolErrorError('Not a valid command datainfo structure')

    def validate_value(self, val):
        """Validates that a value conforms to the type and limits"""
        raise ValueError('not possible to validate a command, only argument or result')


SECOP_TYPES = {
    'double': SecDouble,
    'int': SecInt,
    'bool': SecBool,
    'scaled': SecScaled,
    'array': SecArray,
    'string': SecString,
    'struct': SecStruct,
    'enum': SecEnum,
    'tuple': SecTuple,
    'command': SecCommand
}



if __name__ == "__main__":
    b = '1234'
    b_ = suggest(b, True)
    print(b_)
    print(b_.validate_value(b))
    a = 'test'
    print(suggest(a))
    arr = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    my_array = suggest(arr)
#    my_array.members.update({'type': 'string'})
    print('validated array: ')
    print(my_array.validate_value(arr))
    print(my_array)
    struct = {"int": 45546, "double": 341.65999999983427, "string": "test"}
    my_struct = suggest(struct)
    print(my_struct)
    print(my_struct.validate_value(struct))

    to_add = suggest(1234)
    print(to_add)
    print(my_struct)
    advanced = json.loads('{"int":15035,"double":112.8000000000208,"string":"test","array":[1,2,3,4,5,6,15035]}')
    my_advanced = suggest(advanced)
    print(my_advanced)

    sf = SecDouble({'min':0.05, 'max':99.95})
    sf2 = SecDouble()
    print('valid secfloat: {}'.format(sf2.validate_value(35.6)))
#    si = SecInt(min=0, max=100)
#    sa = SecArray(type='array', members={'type': 'array', 'members': {'type': 'int', 'min_len': 1, 'max_len': 200}, 'max': 7}, max_len=10)
#    int_test_str = '{"type": "array", "members": {"type": "array", "members": {"type": "int", "min": 1, "max": 200}, "max_len":8}, "max_len":7}'
#     tuple_test_str = '{"type":"tuple","members":[{"type":"enum","members":{"DISABLED":0,"IDLE":100,"WARN":200,"UNSTABLE":270,"BUSY":300,"ERROR":400}},{"type":"string"}]}'
#    scaled_test_str = '{"type": "array", "members": {"type": "array", "members": {"type": "scaled", "min": 1, "max": 200, "scale_factor" : 0.1}, "max_len":8}, "max_len":7}'
    enum_test_str = '{"type":"enum","members":{"OFF":0,"ON":1}}'
    struct_test = '{"type": "struct", "members": {"a": {"type" : "int", "min": 1, "max": 200}},"unit":"bananas", "other stuff":"should not be picked upp"}'
#     struct_test = '{"type":"struct","test":"test","members":{"x":{"type":"int","min":1,"max":200},"y":{"type":"array","members":{"type":"array","members":{"type":"scaled","min":0,"max":200,"scale_factor":1},"min_len":2,"max_len":8},"min_len":3,"max_len":7}}}'
    st = SecStruct(json.loads(struct_test))
#    sa2 = SecArray(**json.loads(int_test_str))
#    ss = SecArray(**json.loads(scaled_test_str))
    se = SecEnum(json.loads(enum_test_str))
    print('structure with unit:')
    print(st.structure())
    print(se.structure())

#    print('struct: ' + str(secop_pair_array(**st.structure())))

 #   st_structure = st.structure()
#    print(type(st_structure))
 #   print('st structure: ' + str(st))

#    obj_str = json.loads(struct_test)
 #   print('spa struct: ' + json.dumps(secop_pair_array(dict(json.loads(struct_test)))))
#    print(json.dumps(sf.structure()))
#    print(si)
#    print(json.dumps(si.structure()))
#    print(sa)
#    print(sa2)
#    out = sa.validate_value(json.loads('[[1,201],[2,3],[4],[3,4,5,6.8]]'))
#    print(out)
#    out = ss.validate_value(json.loads('[[1,201],[2,3],[4],[3,4,5,6.8]]'))
 #   print(ss)
 #   print(json.dumps(ss.structure()))
#    print(ss)
#     double_json = '{"type":"double","min":-100,"max":200,"unit":"C"}'
#     print('+' * 20)
#     sd = from_structure(json.loads(double_json))
#     print('-'*20)
#     print(sd)
#     print(json.dumps(sd.structure()))

    # out = st.validate_value(json.loads('{"x": 100, "y": [[1,200],[2,3],[4,5],[3,4,5,6.8]]}'))
    # out = sd.validate_value('34.56')
    # print('validated double ' + str(out))
    # print(st)
    # print(se.validate_value(0))
    # my_int = SecInt(min=0, max=100)
    # my_int.value = 10
    # print(my_int)
    # print(my_int.validate_value('55'))
#     print(json.dumps(my_int.structure()))
#
#     stup = SecTuple(**json.loads(tuple_test_str))
#     stup_test = json.loads('[100, "hej hopp"]')
#     print(stup_test)
#     print(stup.validate_value(stup_test))
#     print(stup)
#     print(st)

    # command_test_string = '{"argument":{"type":"enum","members":{"Off":0,"On":1}},"result":{"type":"enum","members":{"Off":0,"On":1}},"type":"command"}'
    # test = json.loads(command_test_string)
    # print(type(test))
    # ct = from_structure(test)
    # print(ct.argument.validate_value(1))
    # print(ct)
    # print(json.dumps(str(ct)))
    sb = suggest('24.0', parse_from_string=True)
    print(sb)
    print(sb.validate_value('-314'))
    test = b'24.0\r\n'
    print(type(test))
    test.rstrip()
    test = test.decode()
    print(test)
    test2 = dict()
    print(test2)
    if test2:
        print('true')
    else:
        print('false')

    print('-'*20)
    print(type(sb))
    print(isinstance(sb, DataInfo))
