The SECoP Structure library enables parsing of SECoP structure reports with optional Octopy-specific added data.
The library will validate the structure during parsing to make sure it is correct and contains all mandatory fields.
It will however currently not validate that all mandatory accessibles are present such as value, target, status etc.
Once the structure report has been parsed the result is a tree structure of SecNode, Module and Accessible objects
that can be search, edited and manipulated. The structure can then easily be converted into a JSON-formatted structure
report, either in a strict SECoP-compatible format or an internal Octopy-specific format. The two formats are identical with
the exception of a single key in most or all accessibles that contains an object with various extra information such
as topics for communication, hints for the GUI etc.
